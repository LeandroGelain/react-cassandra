import React, { Component } from "react";

import "./App.css";

import { createBrowserHistory } from 'history';

import axios from 'axios'
import { Button } from "@material-ui/core";
class App extends Component {
  
  constructor(props){
    super(props);
    this.state = {
    
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  async handleSubmit(event) {
    const history = createBrowserHistory();
    event.preventDefault();
    let search = window.location.search;
    let params = new URLSearchParams(search);
    let id = params.get('id');
    id = parseInt(id)
    await axios.put(`http://localhost:3333/library/?id=${id}`,this.state)
    history.push('/')
    window.location.reload()
  }

  handleChange(event) {
    let data = {};
    data[event.target.name] = event.target.value;
    this.setState(data)
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <p>Editar livro</p>
          <div className="form">
            <form>
              <input
                className="input"
                type="text"
                name="name_book"
                placeholder="Nome do Livro"
                onChange={this.handleChange}
              />
              <input
                className="input"
                type="text"
                name="author_name"
                onChange={this.handleChange}
                placeholder="Autor"
              />
              <input
                className="input"
                type="text"
                name="gender"
                onChange={this.handleChange}
                placeholder="Gênero"
              />
              <input
                className="input"
                type="text"
                name="sinopse"
                onChange={this.handleChange}
                placeholder="Sinopse"
              />
                <Button href="/" className="input buttons" type="submit" onClick={this.handleSubmit}>Editar</Button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
