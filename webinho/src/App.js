import React, { Component } from "react";

import "./App.css";

import Tables from './components/Tables.js'
import axios from 'axios'
class App extends Component {

  constructor(props){
    super(props);
    this.state = {
    
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  async handleSubmit(event) {
    event.preventDefault();
    await axios.post('http://localhost:3333/library',this.state).then((response)=> {
      window.location.reload()
    })
  }

  handleChange(event) {
    let data = {};
    data[event.target.name] = event.target.value;
    this.setState(data)
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <p>Cadastrar Livros</p>
          <div className="form">
            <form>
              <input
                className="input"
                type="text"
                name="name_book"
                placeholder="Nome do Livro"
                onChange={this.handleChange}
              />
              <input
                className="input"
                type="text"
                name="author_name"
                onChange={this.handleChange}
                placeholder="Autor"
              />
              <input
                className="input"
                type="text"
                name="gender"
                onChange={this.handleChange}
                placeholder="Gênero"
              />
              <input
                className="input"
                type="text"
                name="sinopse"
                onChange={this.handleChange}
                placeholder="Sinopse"
              />
              <input className="input buttons" type="submit" value="Enviar" onClick={this.handleSubmit}/>
            </form>
          </div>
          <p> Livros Cadastrados </p>
          <Tables />
        </div>
      </div>
    );
  }
}

export default App;
