import React, { Component } from "react";
import {
  Button,
  ButtonGroup,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Box,
} from "@material-ui/core";
import axios from 'axios';
import { Link } from "react-router-dom";

class Tables extends Component {

  constructor(props){
    super(props);
    this.state = {
      data:false
    }
    this.deleteRow = this.deleteRow.bind(this)
  }
  async getdata() {
    await axios.get('http://localhost:3333/library').then((response) => {
      this.setState({data:response.data})
    })
  }
  async deleteRow(id){
    await axios.delete('http://localhost:3333/library',{ data: {id}})
    window.location.reload()
  }
  componentDidMount(){
    this.getdata()
    
  };
  handleChange(event) {
    let data = {};
    data[event.target.name] = event.target.value;
    this.setState(data);
  }

  render (){
    return (
      <div className="container">
        <TableContainer>
          <Table className="container">
            <TableHead className="styles">
              <TableRow>
                <TableCell> Nome do Livro </TableCell>
                <TableCell> Autor </TableCell>
                <TableCell> Gênero </TableCell>
                <TableCell> Sinopse </TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              { this.state.data && this.state.data.map((row) => {
                return (
                  <TableRow>
                    <TableCell>{row.name_book}</TableCell>
                    <TableCell>{row.author_name}</TableCell>
                    <TableCell>{row.gender}</TableCell>
                    <TableCell>{row.sinopse}</TableCell>
                    <TableCell>
                    <ButtonGroup variant="text" color="primary" aria-label="text primary button group">
                      <Button variant='text' color="secondary" onClick={() => {this.deleteRow(row.id)}}>Delete</Button>
                      <Link to={`/edit/?id=${row.id}`}>
                        <Button variant='text' color="primary">Edit</Button>
                      </Link>
                    </ButtonGroup>
                    </TableCell>
                  </TableRow>
                  )
                }
              )} 
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    );
  };
}

export default Tables;