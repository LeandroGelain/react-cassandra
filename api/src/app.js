const express = require('express');
const morgan = require('morgan');
const library = require('./routes/libraryRoute');

var cors = require('cors');

const app = express();

app.use(morgan('dev'));

app.use(cors()); 
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use('/library',library);

app.use((req, res, next) => {
    const error = new Error("Route not found (404)");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error : error.message
    });
});

module.exports = app;
