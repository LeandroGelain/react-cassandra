const express = require('express');
const router = express.Router();

const controller = require('../controller/libraryController');

router.get('/', controller.show);
router.post('/', controller.store);
router.put('/', controller.edit);
router.delete('/', controller.delete);

module.exports = router;
