const cassandra = require('cassandra-driver');

const client = new cassandra.Client({ contactPoints: ['localhost'], localDataCenter: 'datacenter1', keyspace:"library" },)

export async function create_table(){
   await client.execute(`CREATE TABLE library.libraries(
    id int PRIMARYKEY,  
    name_book text,  
    author_name text,
    gender text,
    sinopse text
  );`)
}

